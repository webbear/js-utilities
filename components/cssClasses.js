//////////////////////////////
// set classes to the html tag according to the path
//////////////////////////////
function cssClasses(params) {
  params = typeof params === 'undefined' ? {} : params;
  var homepage = params.hasOwnProperty('homepage') ? params.homepage : 'homepage',
  prefix = params.hasOwnProperty('prefix')  ? params.prefix : 'w-',
  langInPath = params.hasOwnProperty('langInPath') ? params.langInPath : false,
  langPrefix = params.hasOwnProperty('langPrefix') ? params.langPrefix : prefix + "lang-";
  
  
  var url = location.pathname.toLowerCase(),
  segments = url.split('/').filter(function(entry) { return entry.trim() != ''; });
  var html = document.getElementsByTagName('html')[0];
 
  if (segments.length) {
    segments.forEach(function(v,k) {
      var cl = v.replace(/\./g, '-').replace(/(\%20)/g, '-');
      if (langInPath && k ===  0) {
        if (segments.length === 1) {
          html.classList.add(prefix+homepage);
          html.classList.add(langPrefix+cl);
        } else {
          html.classList.add(langPrefix+cl);
        }
      } else {
        html.classList.add(prefix+cl);
      }
      
    });
  } else {
    html.classList.add(prefix+homepage);
  }
}