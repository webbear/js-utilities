///////////////////////
// light weight browser detection
//////////////////////

function testBrowser() {
  var ua = navigator.userAgent;
  var out = 'unknown-browser';
  if (/Safari/i.test(ua)) {
    out = 'safari';
    if (/iPhone/i.test(ua)) {
      out +=' iphone';
    }
    if (/iPad/i.test(ua)) {
      out +=' ipad';
    }
    if (/Macintosh/i.test(ua)) {
      out += " macintosh";
    }
    if (/Windows/i.test(ua)) {
      out += " windows";
    }
  }
  if (/Chrome/i.test(ua)) {
		if (/Edge/i.test(ua)) {
			out = "edge";
		} else {
			out = 'chrome';
		}
    
    if (/Macintosh/i.test(ua)) {
      out += " macintosh";
    }
    if (/Windows/i.test(ua) && !/mobi/i.test()) {
      out += " windows";
    }
    if (/Linux/i.test(ua)) {
      out += " linux";
    }
    if (/mobi/i.test(ua)) {
      out += " mobile-chrome";
      if (/Windows Phone/i.test(ua)) {
        out += " windows-phone";
      }
      if (/Android/i.test(ua)) {
        out += " android";
      }
    }
  }
  if (/Opera/i.test(ua)) {
    out = 'opera';
    if (/Opera Mini/i.test(ua)){
      out += 'opera-mini';
    }
    if (/Opera Mobi/i.test(ua)){
      out += 'opera-mobile';
    }
  }
  if (/MSIE 8/i.test(ua)) {
    out = 'ie ie8';
  }
  if (/MSIE 9/i.test(ua)) {
    out = 'ie ie9';
    if (/Windows Phone/i.test(ua)) {
      out = "mobile-ie ie9";
    }
  }
  if (/MSIE 10/i.test(ua)) {
    out = 'ie ie10';
  }
  if (/rv:11\.0/i.test(ua)) {
    if (/Trident\/7\.0; rv:11\.0/i.test(ua)) {
      out = 'edge';
    } else {
      out = 'ie ie11';
    }
    
  }
  if (/Edge/i.test(ua)) {
    out = 'edge';
  }
  return out;
}
