if (!Array.prototype.filter) {
  Array.prototype.filter = function(fun/*, thisArg*/) {
    'use strict';

    if (this === void 0 || this === null) {
      throw new TypeError();
    }

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== 'function') {
      throw new TypeError();
    }

    var res = [];
    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
    for (var i = 0; i < len; i++) {
      if (i in t) {
        var val = t[i];
        if (fun.call(thisArg, val, i, t)) {
          res.push(val);
        }
      }
    }

    return res;
  };
}

var _u = {
  //////////////////////////////
  // helpers
  ////////////////////////////// 
  h: {
    addClass: function(el,className) {
      if (el.classList) {
        el.classList.add(className);
      } else {
        el.className += ' ' + className;
      }      
    },
    removeClass: function(el, className) {
      if (el.classList)
        el.classList.remove(className);
      else
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    },
    insertAfter: function(el, htmlString){
      el.insertAdjacentHTML('afterend', htmlString);
    },
    append: function(parent, element) {
      console.log(parent)
      parent.appendChild(element);
    },
    prepend: function(el, htmlString) {
      el.insertAdjacentHTML('beforebegin', htmlString);
    },
    clone: function(el) {
      el.cloneNode(true);
    },
		extend: function(out) {
		  out = out || {};

		  for (var i = 1; i < arguments.length; i++) {
		    if (!arguments[i])
		      continue;

		    for (var key in arguments[i]) {
		      if (arguments[i].hasOwnProperty(key))
		        out[key] = arguments[i][key];
		    }
		  }

		  return out;
		},
		toggleClass: function(el, className) {

			if (el.classList) {
			  el.classList.toggle(className);
			} else {
			  var classes = el.className.split(' ');
			  var existingIndex = classes.indexOf(className);

			  if (existingIndex >= 0)
			    classes.splice(existingIndex, 1);
			  else
			    classes.push(className);

			  el.className = classes.join(' ');
			}
		}
  },
  
  //////////////////////////////
  // main methods
  //////////////////////////////
  isMobile: function() {
    var mobile = (navigator.userAgent.match(/Mobi/i) === null) ? false : true;
    return mobile;
  },
  testBrowser: function() {
    var ua = navigator.userAgent;
    var out = 'unknown-browser';
    if (/Safari/i.test(ua)) {
      out = 'safari';
      if (/iPhone/i.test(ua)) {
        out +=' iphone';
      }
      if (/iPad/i.test(ua)) {
        out +=' ipad';
      }
      if (/Macintosh/i.test(ua)) {
        out += " macintosh";
      }
      if (/Windows/i.test(ua)) {
        out += " windows";
      }
    }
    if (/Chrome/i.test(ua)) {
  		if (/Edge/i.test(ua)) {
  			out = "edge";
  		} else {
  			out = 'chrome';
  		}
    
      if (/Macintosh/i.test(ua)) {
        out += " macintosh";
      }
      if (/Windows/i.test(ua) && !/mobi/i.test()) {
        out += " windows";
      }
      if (/Linux/i.test(ua)) {
        out += " linux";
      }
      if (/mobi/i.test(ua)) {
        out += " mobile-chrome";
        if (/Windows Phone/i.test(ua)) {
          out += " windows-phone";
        }
        if (/Android/i.test(ua)) {
          out += " android";
        }
      }
    }
    if (/Opera/i.test(ua)) {
      out = 'opera';
      if (/Opera Mini/i.test(ua)){
        out += 'opera-mini';
      }
      if (/Opera Mobi/i.test(ua)){
        out += 'opera-mobile';
      }
    }
    if (/MSIE 8/i.test(ua)) {
      out = 'ie ie8';
    }
    if (/MSIE 9/i.test(ua)) {
      out = 'ie ie9';
      if (/Windows Phone/i.test(ua)) {
        out = "mobile-ie ie9";
      }
    }
    if (/MSIE 10/i.test(ua)) {
      out = 'ie ie10';
    }
    if (/rv:11\.0/i.test(ua)) {
      if (/Trident\/7\.0; rv:11\.0/i.test(ua)) {
        out = 'edge';
      } else {
        out = 'ie ie11';
      }
    
    }
    if (/Edge/i.test(ua)) {
      out = 'edge';
    }
    return out;
  },
  cssClasses: function(params) {
    params = typeof params === 'undefined' ? {} : params;
    var homepage = params.hasOwnProperty('homepage') ? params.homepage : 'homepage',
    prefix = params.hasOwnProperty('prefix')  ? params.prefix : 'w-',
    langInPath = params.hasOwnProperty('langInPath') ? params.langInPath : false,
    langPrefix = params.hasOwnProperty('langPrefix') ? params.langPrefix : prefix + "lang-";
  
  
    var url = location.pathname.toLowerCase(),
    segments = url.split('/').filter(function(entry) { return /\S/.test(entry); });
    var html = document.getElementsByTagName('html')[0];
 
    if (segments.length) {
      
      var i = 0, segLen = segments.length;
      
      for (; i < segLen; i++) {
        var cl = segments[i].replace(/\./g, '-').replace(/(\%20)/g, '-');
        if (langInPath && i ===  0) {
          if (segLen === 1) {
            _u.h.addClass(html, prefix+homepage);
           
            _u.h.addClass(html, prefix+langPrefix+cl);
           
          } else {
           
            _u.h.addClass(html, langPrefix+cl);
          }
        } else {
         
          _u.h.addClass(html, prefix+cl);
        }
      } 
    } else {
      _u.h.addClass(html, prefix+homepage);
     
    }
  }
}